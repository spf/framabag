<?php

namespace Wallabag\FramaBundle\Event\Subscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class CheckArticlesSubscriber
{
    private $router;
    private $em;
    private $tokenStorage;
    private $logger;
    private $enabled;
    private $nbArticles;

    public function __construct(Router $router,
                                EntityManager $em,
                                TokenStorageInterface $tokenStorage,
                                LoggerInterface $logger,
                                $enabled,
                                $nbArticles)
    {
        $this->router = $router;
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
        $this->enabled = $enabled;
        $this->nbArticles = $nbArticles;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $currentPath = $event->getRequest()->getPathInfo();

        $canContinue = true;

        $this->logger->info($currentPath);

        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        // If the user is not logged
        if (null === $user || !is_object($user)) {
            $canContinue = false;
        }

        // To skip the render controllers
        if (false === $event->isMasterRequest()) {
            return;
        }

        if ($currentPath === '/new-entry' && $canContinue) {
            // We check if the user has an active subscription
            $nbEntries = $this->em->getRepository('WallabagCoreBundle:Entry')
                ->countAllEntriesByUser($user->getId());
            return $this->interfaceRestrict($event, $nbEntries);
        }
        if ($currentPath === '/api/entries' && $event->getRequest()->getMethod() === 'POST' && $canContinue) {
            // We check if the user has an active subscription
            $nbEntries = $this->em->getRepository('WallabagCoreBundle:Entry')
                ->countAllEntriesByUser($user->getId());
            return $this->apiRestrict($event, $nbEntries);
        }
    }

    private function interfaceRestrict(FilterControllerEvent $event, $nbEntries)
    {
        if ($nbEntries > $this->nbArticles) {
            $redirectUrl = $this->router->generate('restrict_articles_index');
            $event->setController(function () use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
            });
        }
    }

    private function apiRestrict(FilterControllerEvent $event, $nbEntries)
    {
        if ($nbEntries > $this->nbArticles) {
            $event->setController(function () {
                return new JsonResponse([], 403);
            });
        }
    }
}
