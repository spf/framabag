<?php

namespace Wallabag\FramaBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class WallabagFramaExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $container->setParameter('frama.restrict.enabled', $config['restrict']['enabled']);
        $container->setParameter('frama.restrict.nb_entries', $config['restrict']['nb_entries']);
    }

    public function getAlias()
    {
        return 'wallabag_frama';
    }
}
